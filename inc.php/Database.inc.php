<?php

/**
 * Created by PhpStorm.
 * User: robben1
 * Date: 23.11.15
 * Time: 16:42
 */

include 'Error.inc.php';
class Database
{
    function __construct()
    {
        try {
            $this->pdo = new PDO('mysql:host=localhost;dbname=order', 'root', '1234');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->exec('SET NAMES "utf8"');
        } catch (PDOException $e) {
            Error::thisIsTheEnd($e);
        }
    }

    function authorize($uid, $password)
    {
        try {
            $sql = 'INSERT INTO users(date, started, ended, password, uid) VALUES (:date, :started, :ended,:password, :uid)';
            $s = $this->pdo->prepare($sql);

            $s->execute([
                ':date' => date('Y-m-d H:i:s'),
                ':started' => date('H:i:s'),
                ':ended' => date('H:i:s'),
                ':password' => $password,
                ':uid' => $uid
            ]);

        } catch (PDOException $e) {
            Error::thisIsTheEnd($e);
        }
    }

    function deauthorize($uid)
    {
        try {
            $sql = 'UPDATE users SET ended = :ended WHERE uid = :uid AND started = ended';
            $s = $this->pdo->prepare($sql);
            $s->execute([
                ':ended' => date('H:i:s'),
                ':uid' => $uid
            ]);
        } catch (PDOException $e) {
            Error::thisIsTheEnd($e);
        }
    }

    function outputAdmin($fromDate,$toDate)
    {
        // от даты fromDate до даты toDate выводим
		$sql = 'SELECT surname, uid, started, ended, date FROM users INNER JOIN surnames AS s WHERE (date >= :fromDate AND date <= :toDate) OR (date = :fromDate AND date = :toDate) AND surname = uid';
		$s = $this->pdo->prepare($sql);
        $s->execute([
            ':fromDate' => $fromDate,
			':toDate' => $toDate
        ]);

        $result = $s->fetchAll();
        return $result;
    }

    function outputSumAdmin($fromDate,$toDate,$uid){
		//сумма от даты до даты проработаного времени для каждого робочего
		$sqlSum = 'SELECT uid, (HOUR(sum)-HOUR(sum)%24)/24 as day, HOUR(sum)%24 as hour, MINUTE(sum) as minute FROM(SELECT uid, SEC_TO_TIME(SUM( TIME_TO_SEC(ended)-TIME_TO_SEC(started))) as sum FROM users WHERE uid = :uid AND date > :fromDate AND date < :toDate) as sum;';
		$s = $this->pdo->prepare($sqlSum);
		$s->execute([
			':fromDate' => $fromDate,
			':toDate' => $toDate,
			':uid' => $uid
		]);
		$result = $s->fetchAll();

		return $result;
	}

    function  outputUser()
    {
        $date = date('Y-m-d H:i:s');
        $sql = 'SELECT started, ended, uid FROM users WHERE YEAR(date) = YEAR(:date) AND MONTH(date) = MONTH(:date) AND DAY(date) = DAY(:date) AND uid = :uid';
        $s = $this->pdo->prepare($sql);
        $s->execute([
            ':date' => $date,
            ':uid' => $_POST['id']
        ]);

        $result = $s->fetchAll();
        return $result;

    }

    function passwordCheck($login, $password)
    {
        try {
            $sql = 'SELECT password FROM users WHERE uid = :id';
            $s = $this->pdo->prepare($sql);
            $s->execute([
                ':id' => $login
            ]);

            $result = $s->fetchAll();
            if ($result[0]['password'] == $password) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            Error::thisIsTheEnd($e);
        }

    }
    
     function checkYesterday($login) 
    { 
         
        $sql = 'SELECT 1 FROM users WHERE uid = :id AND YEAR(date) = YEAR(:date) AND MONTH(date) = MONTH(:date) AND DAY(date) = DAY(:date)-1 AND started = ended'; 
        $s = $this->pdo->prepare($sql); 
        $s->execute([ 
        ':id' => $login, 
        ':date' => date('Y-m-d H:i:s'), 
        ]); 

        $result = $s->fetchAll(); 
        if($result[0])
        { //если селект вернул 1 
        //дать возможность ввести (инсертнуть во вчерашний ended) время когда ушел 
        //записать в penalty 1 

            echo "Введите время когда закончили роботу вчера";
            include '/html.php/yesterday.html.php';
            if(isset($_POST['ytime']))
            {
               
                $sql = 'UPDATE users SET ended = :ended WHERE uid = :id AND YEAR(date) = YEAR(:date) AND MONTH(date) = MONTH(:date) AND DAY(date) = DAY(:date)-1 AND started = ended';
                $s = $this->pdo->prepare($sql);
                $s->execute([
                    ':ended' => $_POST['ytime'],
                    ':id' => $login, 
                    ':date' => date('Y-m-d H:i:s')           

                ]);
                 unset($_POST['ytime']);
                 $result[0]=false;


            } 
            
        }

    }
}