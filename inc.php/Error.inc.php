<?php
/**
 * Created by PhpStorm.
 * User: robben1
 * Date: 23.11.15
 * Time: 16:45
 */

class Error extends PDOException
{
    static function thisIsTheEnd($e) {
        $error = 'Error: ' . $e->getMessage();
        include '/html.php/error.html.php';
        exit();
    }
}