function checkForm(frome){
	var n=0;
	var reg = /^[0-9]{4}-(0[1-9]|10|11|12)-(0[1-9]|[1,2][0-9]|30|31)$/g;
	var from=document.getElementById('from');
	var to=document.getElementById('to');
	if ((from.value=="")|| !(reg.test(from.value))){
		from.style.borderColor = '#e9322d';
		from.style.boxShadow = '0 0 6pt red';
		document.getElementById('search').style.display='none';
    }else{
		from.style.borderColor = 'green';
		from.style.boxShadow = '0 0 0px red';
		n++;
	}
	reg = /^[0-9]{4}-(0[1-9]|10|11|12)-(0[1-9]|[1,2][0-9]|30|31)$/g;
	if ((to.value=="")|| !(reg.test(to.value))){
		to.style.borderColor = '#e9322d';
		to.style.boxShadow = '0 0 6pt red';
		document.getElementById('search').style.display='none';
		return false;
    }else{
		to.style.borderColor = 'green';
		to.style.boxShadow = '0 0 0px red';
		n++;
	}
	if(n==2){
		document.getElementById('search').style.display='inline-block';
		return true;
	}
}
function date(){
	var date = new Date();
	var year=date.getFullYear();
	var month=date.getMonth()+1;
	var day=date.getDate();
	document.getElementById('from').value=year+"-"+month+"-"+day;
	document.getElementById('to').value=year+"-"+month+"-"+day;
}

$(document).ready({

});