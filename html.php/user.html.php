<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Работа</title>

</head>
<body id="userfile">

<form action="?" method="post">
    <div id="userControls">
        <input type="submit" id="auth" name="auth" value="   Начать работу   " class="btn btn-success btn-lg">
        <input type="submit" id="deauth" name="deauth" value="Закончить работу" class="btn btn-danger btn-lg">
    </div>
</form>

<div>
    <?php

    $result = $db->outputUser();
    echo 'Информация по пользователю: ' . $result[0]['uid'];

    $ended = '';
    if ($result[count($result) - 1]['ended'] == $result[count($result) - 1]['started']) {
        $ended = 'работаю';
    } else {
        $ended = $result[count($result) - 1]['ended'];//вывод времини последнего в одном дне
    }

    echo '<p>Начал работу в: ' . $result[0]['started'] . '</p><p>Закончил в: ' . $ended . '</p>';

    ?>
</div>

</body>
</html>